const path = require('path');
const crypto = require('crypto');
const favicon = require('serve-favicon');
const compress = require('compression');
const helmet = require('helmet');
const CronJob = require('cron').CronJob;
const cors = require('cors');
const logger = require('./core/logger');
const enums = require('./core/enums');

const feathers = require('@feathersjs/feathers');
const configuration = require('@feathersjs/configuration');
const express = require('@feathersjs/express');
const socketio = require('@feathersjs/socketio');

const middleware = require('./middleware');
const services = require('./services');
const appHooks = require('./app.hooks');
const channels = require('./core/channels');
const mongoose = require('./core/mongoose');
const twilio = require('./core/twilio');
const payments = require('./core/payments');
const utils = require('./core/utils');
const storage = require('./core/storage');
const tasks = require('./core/tasks');
const notifications = require('./core/notifications');

const app = express(feathers());
app.set('enums', enums);

// Load app configuration
app.configure(configuration());

// Enable security, CORS, compression, favicon and body parsing
app.use(helmet());
app.use(cors());
app.use(compress());
// app.use(express.json());
// app.use(express.urlencoded({ extended: true }));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));

// Set up Plugins and providers
app.configure(express.rest());

const wsPath = app.get('apiPath') + '/api/ws';
app.configure(socketio({ path: wsPath }, io =>
{
    io.on('connection', function(socket)
    {
        logger.title(`[${socket.feathers.userCode}]: WebSocket Connected [${wsPath}]`);
        //socket.emit('news', { text: 'A client connected!' });
        //socket.on('my other event', function (data) {console.log(data);});

        socket.on('disconnect', function()
        {
            logger.title(`[${socket.feathers.userCode}]: WebSocket Disconnected`);
        });

    });

    io.use((socket, next) =>
    {
        const userIp = socket.request.headers['x-real-ip'] || '?';
        const finger = userIp + '|' + socket.handshake.headers['user-agent'];
        const userCode = crypto.createHash('md5').update(finger).digest('hex').substring(0, 8);
        logger.title(`[${userCode}]: WebSocket Request: Ip: [${userIp}] [${socket.id}]`);
        // console.dir(socket.handshake, { depth: null });
        socket.feathers.remoteIp = userIp;
        socket.feathers.userCode = userCode;
        socket.feathers.referrer = socket.request.headers.referer;

        // socket.feathers.handshake = socket.handshake;
        next();

        /*{  HANDSHAKE:
            headers: {
              'upgrade': 'websocket',
              'connection': 'upgrade',
              'host': 'eng-app.testhorses.com',
              'x-real-ip': '46.34.227.135',
              'x-forwarded-for': '46.34.227.135',
              'pragma': 'no-cache',
              'cache-control': 'no-cache',
              'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.Safari/537.36',
              'origin': 'https://eng-app.testhorses.com',
              'sec-websocket-version': '13',
              'accept-encoding': 'gzip, deflate, br',
              'accept-language': 'sk-SK,sk;q=0.9,cs;q=0.8,en-US;q=0.7,en;q=0.6',
              'sec-websocket-key': 'CzZBIau+PoDeUIakAY7/oQ==',
              'sec-websocket-extensions': 'permessage-deflate; client_max_window_bits'
            },
            time: 'Wed Nov 18 2020 18:29:33 GMT+0100 (Central European Standard Time)',
            address: '::ffff:127.0.0.1',
            xdomain: true,
            secure: false,
            issued: 1605720573229,
            url: '/api/ws/?EIO=3&transport=websocket',
            query: { EIO: '3', transport: 'websocket' }
        } */
    });
}));

app.configure(mongoose);
app.configure(twilio);
app.configure(payments);
app.configure(utils);
app.configure(storage);
app.configure(tasks);
app.configure(notifications);


// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
//app.configure(authentication);

// Set up our services (see `services/index.js`)
app.configure(services);

// Set up event channels (see channels.js)
app.configure(channels);

// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(express.errorHandler({ logger }));

// set hooks
app.hooks(appHooks);

// initialize subsystems
const initialize = async function()
{

    // setup notifications
    const Notifications = app.get('notifyUtils');
    await Notifications.initialize();

    // setup payments
    const PaymentsUtils = app.get('paymentsUtils');
    await PaymentsUtils.initialize();

    // setup tasks
    const taskDefs = app.get('tasks');
    const job1 = new CronJob('30 */5 * * * *', taskDefs.processFinishedClasses);
    job1.start();
    const job2 = new CronJob('0 20,50 * * * *', taskDefs.classReminderChecker);
    job2.start();
    logger.title('::: Cronjobs started');
}

initialize().catch(logger.error);
module.exports = app;
