
const fs = require('fs');
const path = require('path');
const unzipper = require('unzipper');
const { AirParser } = require("airppt-parser");
const logger = require('./logger');

module.exports = class Presentation
{
    constructor()
    {
        this.file = '';
        this.slides = [];
        this.images = [];
    }

    fileExists(path)
    {
        return new Promise(resolve =>
        {
            fs.exists(path, resolve);
        });
    }

    convertHexColor(color)
    {
        return `#${color}`;
    }

    unitToPixel(emu)
    {
        emu = emu ? Number(emu) : 0;
        return Math.floor(emu / 12700);
    }

    fontToPixel(size)
    {
        size = size ? Number(size) : 0;
        return Math.floor(size / 480);
    }

    sizeToPixel(size)
    {
        size = size ? Number(size) : 0;
        return Math.floor(size / 100);
    }

    getParagraphProps(paragraph_item)
    {
        const props = {};
        const propList = paragraph_item['a:pPr'] || [];
        const propItem = propList[0];
        if (propItem)
        {
            props.indent = this.unitToPixel(propItem['$'] ? propItem['$'].indent : 0);
            props.align = propItem['$'] ? propItem['$'].algn : '';
            props.fontAlign = propItem['$'] ? propItem['$'].fontAlgn : '';
            props.bullet = propItem['a:buChar'] ? propItem['a:buChar'][0]['$'].char : '';
        }
        return props;
    };

    getRowText(paragraph_item)
    {
        const parts = [];
        paragraph_item['a:r'].forEach(p_row =>
        { // concat all text parts
            const partial_text = p_row['a:t'];
            parts.push(partial_text);
        });
        const string = parts.join(' ');
        return string.replace(/\s\s+/g, ' ');
    };

    getRowStyle(paragraph_item)
    {
        const style = {};
        paragraph_item['a:r'].forEach(p_row =>
        { // get all styles
            const rowProps = p_row['a:rPr'] ? p_row['a:rPr'][0] : {};
            if (rowProps['$'])
            {
                const attr = rowProps['$'];
                if (attr.sz)        style.fontSize = this.fontToPixel(attr.sz) + 'px';
                if (attr.b)         style.fontWeight = (attr.b === '1') ? 'bold' : 'normal';
                if (attr.i)         style.fontStyle = (attr.i === '1') ? 'italic' : 'normal';
                if (attr.strike)    style.textDecoration = (attr.strike === 'noStrike') ? 'none' : 'line-through';
                if (attr.u)         style.textDecoration = (attr.u === 'none') ? 'none' : 'underline';
            }
        });
        return style;
    };

    processParagraph(paragraph)
    {
        const rows = [];
        paragraph.forEach(p_item =>
        { // iterate all items in 'a:p', possible subkeys: 'a:pPr' / 'a:r'
            if (p_item['a:r'])
            { // rows exists
                const p_props = this.getParagraphProps(p_item);
                const p_text = this.getRowText(p_item);
                const p_style = this.getRowStyle(p_item);

                let text_before = '';
                if (p_props.bullet)
                { // bullets
                    if (p_props.bullet === 'Ø') text_before = '⮚ ';
                    else                        text_before = '- ';
                }

                const row = {};
                row.style = p_style;
                row.text = text_before + p_text;
                // row.props = p_props;
                rows.push(row);
            }
        });
        return rows;
    };

    extractBlocks(element)
    {
        const blocks = [];
        const raw = element.raw;

        // text:  'p:nvSpPr', 'p:spPr', 'p:style', 'p:txBody'
        // image: 'p:nvPicPr', 'p:blipFill', 'p:spPr'

        if (raw['p:nvPicPr'])
        { // image
            const block = {};
            block.type = 'image';
            block.uri = element.links.Uri || '';
            blocks.push(block);
            if (block.uri)  this.images.push(block.uri);
        }
        else if (raw['p:txBody'])
        { // text

            const bodyEl = raw['p:txBody'][0];
            const paragraph = bodyEl['a:p'];
            const block = {};
            block.type = 'text';
            block.rows = this.processParagraph(paragraph);
            blocks.push(block);
        }

        return blocks;
    };

    processElement(element)
    {
        const { name, shapeType, specialityType, paragraph, shape, links } = element;
        const { x, y } = element.elementPosition;
        const { cx, cy } = element.elementOffsetPosition;
        const px = this.unitToPixel(x);
        const py = this.unitToPixel(y);
        const ox = this.unitToPixel(cx);
        const oy = this.unitToPixel(cy);

        const debug = {};
        debug.shapeType = shapeType;
        debug.speciality = specialityType;
        debug.paragraph = paragraph;
        debug.shape = shape;
        debug.links = links;

        const style = {};
        if (paragraph && paragraph.textCharacterProperties && paragraph.textCharacterProperties.fillColor)
        { // section text color
            style.color = this.convertHexColor(paragraph && paragraph.textCharacterProperties && paragraph.textCharacterProperties.fillColor);
        }
        if (shape && shape.fill && shape.fill.fillColor)
        { // section background color
            if (shape.fill.fillColor !== 'FFFFFF')
            {
                style.backgroundColor = this.convertHexColor(shape.fill.fillColor);
            }
        }
        if (shape && shape.border)
        { // section border
            const bsize = this.unitToPixel(shape.border.thickness);
            style.border = `${bsize / 2}px ${shape.border.type} ${this.convertHexColor(shape.border.color)}`;
        }

        const result = {};
        result.px = px;
        result.py = py;
        result.ox = ox;
        result.oy = oy;
        // result.debug = debug;
        result.style = style;
        result.blocks = this.extractBlocks(element);
        return result;
    };

    async processPage(pageNr, pageData)
    {
        logger.info(`Processing page: ${pageNr}...`);
        const page = {};
        const content = [];
        for (let i=0; i < pageData.powerPointElements.length; i += 1)
        { // iterate all elements
            const element = pageData.powerPointElements[i];
            const el = await this.processElement(element);
            if (el)  content.push(el);
        };
        content.sort((a, b) => (a.py > b.py) ? 1 : -1);

        page.page = pageNr;
        page.content = content;
        return page;
    };

    async parsePage(parser, page)
    {
        const pageData = await parser.ParsePowerPoint(page).catch(err => null);
        const pageStruct = pageData ? await this.processPage(page, pageData) : null;
        return pageStruct;
    };

    async parse(pptxFile)
    {
        let result = null;
        const exists = await this.fileExists(pptxFile);
        if (exists)
        { // pptx exists, parse...
            try
            {
                this.file = pptxFile;
                logger.info('Loading pptx file:'+ pptxFile);
                result = {};
                const slides = [];
                const pptParser = new AirParser(pptxFile);
                for (let nr = 1; nr < 100; nr += 1)
                {
                    const data = await this.parsePage(pptParser, nr);
                    if (data)   slides.push(data);
                    else        break;
                }
                this.slides = slides;
                this.images = [...new Set(this.images)];
                result.slides = this.slides;
                result.images = this.images;
                logger.info('Pptx parse completed...');
            }
            catch(e)
            {
                result = null;
            }
        }
        return result;
    };

    async storeImages(storage, groupId, itemId, imageList)
    {
        const mapImages = {};

        const zip = fs.createReadStream(this.file).pipe(unzipper.Parse({forceStream: true}));
        for await (const entry of zip)
        {
            const filename = entry.path;
            const ext = path.extname(filename).toLowerCase();
            const validExtensions = ['.png', '.jpg', '.svg'];
            if (imageList.includes(filename) && validExtensions.includes(ext))
            {
                const content = await entry.buffer();
                console.log('Image loaded:', filename, content.length);
                const fileId = 'PPTXIMAGE_' + filename;
                const relPath = content ? await storage.writeBuffer(groupId, itemId, fileId, path.basename(filename), content) : '';
                mapImages[filename] = relPath;
            }
            else
            {
                entry.autodrain();
            }
        }
        return mapImages;
    }

};
