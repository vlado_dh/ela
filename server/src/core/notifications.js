
const logger    = require('./logger');
const PushNotifications = require('node-pushnotifications');


module.exports = function(app)
{
    const notifications = {};
    let service;

    notifications.initialize = function()
    {
        // get settings from config
        const settings = app.get('notifications');
        const apnKeyBuffer = settings.apn.token.key;
        settings.apn.token.key = Buffer.from(apnKeyBuffer, 'hex');
        service = new PushNotifications(settings);
        logger.title('::: Notifications initialized');
        return true;
    };

    notifications.send = async function(ids, data)
    {
        let result = false;
        if (service)
        {
            console.log('Sending notification:');
            result = await service.send(ids, data).then(res => true).catch(err => false);
        }
        return result;
    };

    app.set('notifyUtils', notifications);
};
