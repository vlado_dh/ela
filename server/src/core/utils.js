
const logger    = require('./logger');
const axios     = require('axios');
const dayjs     = require('dayjs');
const stringReplace = require('expand-template')();
const captchaUrl = 'https://www.google.com/recaptcha/api/siteverify';

module.exports = function(app)
{
    const utils = {};

    utils.getUserCredits = async function(userId)
    {
        let credits = false;
        const mongoose = app.get('mongoose');
        const Payment = app.service('payments').Model;
        const currentTime = dayjs();
        const agg = [];
        agg.push({$match: { user: mongoose.Types.ObjectId(userId), state: 'confirmed', $or: [{validTo: null}, {validTo: {$gte: currentTime.toDate()}}] }});
        agg.push({$group: { _id : null, credits: { $sum: '$credits' } }});
        agg.push({$project: {_id: 0, credits: 1} });
        const result = await Payment.aggregate(agg).catch(console.error);
        credits = result && result[0] && result[0].credits || false;
        logger.warn(`[UTILS]: USER: [${userId}] -> CALC AVAILABLE CREDITS: [${credits}]`);
        return credits;
    };

    utils.getPaymentId = async function(authUserId, priceValue)
    {
        const mongoose = app.get('mongoose');
        const currentTime = dayjs();
        const Payment = app.service('payments').Model;
        const query = { user: mongoose.Types.ObjectId(authUserId), credits: { $gte: priceValue }, validTo: {$gte: currentTime.toDate() } };
        const payments = await Payment.find(query).sort({ validTo: 'asc'}).lean().exec();
        const payment = payments[0];
        return payment ? payment._id : '';
    };

    utils.calcWeekCode = function(date)
    {
        const currentTime = dayjs(date);
        const weekNum = ("00" + currentTime.week()).slice(-2);
        const weekCode = Number(currentTime.year() + weekNum);
        return weekCode;
    };

    utils.asyncReplace = async function(string, regexpr, f)
    {
        const matchPromises = {}
        string.replace(regexpr, function(match)
        {
            if (!matchPromises[match])  matchPromises[match] = f(match);
        });
        for (let match in matchPromises)
        {
            matchPromises[match] = await matchPromises[match]
        };
        return string.replace(regexpr, function(match)
        {
            return matchPromises[match];
        });
    };

    utils.evalTemplate = function(template, params)
    {
        return stringReplace(template, params);
    };

    utils.capitalize = function(text)
    {
        return text ? `${text[0].toUpperCase()}${text.slice(1)}` : '';
    };

    utils.verifyCaptchaToken = async function(token)
    {
        const config = app.get('captcha');
        const payload = new URLSearchParams();
        payload.append('secret', config.secretKey);
        payload.append('response', token);
        const result = await axios.post(captchaUrl, payload).catch(err => null);
        return result && result.data || null;
    }

    app.set('utils', utils);
};
