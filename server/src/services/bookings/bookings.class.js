const { Service } = require('feathers-mongoose');
const logger = require('../../core/logger');
const validator = require('../../core/validator');

exports.Bookings = class Bookings extends Service
{
    update(id, data, params)
    {
        data.updatedAt = new Date();
        return super.update(id, data, params);
    };
};
