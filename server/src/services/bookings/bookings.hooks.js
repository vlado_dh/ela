
const logger = require('../../core/logger');
const validator = require('../../core/validator');
const dayjs = require('dayjs');
const { logDebug, disallowApi, authenticateUser } = require('../../hooks/sharedHooks');

const populates = [
    { path: '_teacher_', select: 'email firstName lastName rating avatar' },
    { path: '_student_', select: 'email firstName lastName rating avatar' },
    { path: '_topic_', select: 'title description icon level' }
];

const populates3 = [
    { path: '_topic_', select: 'title description icon' }
];

const selects = ['title', 'description', 'difficulty', 'state', 'startAt', 'endAt', 'teacher', 'student', 'topic', 'confirmed', 'paid', 'completed', 'comment', 'rating', 'seenTimes'];
const selects2 = ['title', 'description', 'difficulty', 'state', 'startAt', 'endAt', 'teacher', 'student', 'topic', 'confirmed', 'paid', 'completed', 'comment', 'price'];
const selects3 = ['title', 'description', 'difficulty', 'state', 'startAt', 'endAt', 'teacher', 'student', 'topic'];

const findTeacherBookings = function()
{
    return async ctx =>
    {
        const { query = {}, connection } = ctx.params;
        if (query.mode === 'teacher')
        {
            const authUser = ctx.params.user;
            logger.info(`[${connection.userCode}]: [BOOKING]: FIND (T): [${authUser._id}]`);
            const q = {query: {}};
            q.query.state = query.state;
            // q.query.completed = false;
            q.query.teacher = authUser._id;
            q.query.$select = selects;
            q.query.$limit = 1000;
            q.query.$sort = { startAt: 1 };
            q.query.$populate = populates;
            const bookings = await ctx.service._find(q).catch(err => null);
            ctx.result = bookings;
            logger.info(`[${connection.userCode}]: [BOOKING]: FIND (T): RESPONSE [${bookings && bookings.data.length || 0} bookings]`);
        }
        return ctx;
    }
}

const findStudentBookings = function()
{
    return async ctx =>
    {
        const { query = {}, connection } = ctx.params;
        if (query.mode === 'student')
        {
            const authUser = ctx.params.user;
            logger.info(`[${connection.userCode}]: [BOOKING]: FIND (S): [${authUser._id}]`);
            const q = {query: {}};
            q.query.state = query.state;
            // q.query.completed = false;
            q.query.$select = selects;
            q.query.student = authUser._id;
            q.query.$limit = 1000;
            q.query.$sort = { startAt: 1 };
            q.query.$populate = populates;
            const bookings = await ctx.service._find(q).catch(err => null);
            ctx.result = bookings;
            logger.info(`[${connection.userCode}]: [BOOKING]: FIND (S): RESPONSE [${bookings && bookings.data.length || 0} bookings]`);
        }
        return ctx;
    }
}

const findHistory = function()
{
    return async ctx =>
    {
        const { query = {}, connection } = ctx.params;
        if (query.mode === 'history')
        {
            const ENUM = ctx.app.get('enums');
            const authUser = ctx.params.user;
            const otherUserId = query.user || '';
            logger.info(`[${connection.userCode}]: [BOOKING]: FIND HISTORY: USER: [${authUser._id}]`);
            const q = {query: {}};
            if (authUser.role === ENUM.USERROLE.STUDENT)
            {
                q.query.$or = [{ teacher: otherUserId}, {student: authUser._id}];
            }
            else if (authUser.role === ENUM.USERROLE.TEACHER)
            {
                q.query.$or = [{ teacher: authUser._id}, {student: otherUserId}];
            }
            else
            {
                q.query.$or = [{ teacher: '?'}, {student: '?'}];
            }
            q.query.endAt = {$lt: new Date()},
            q.query.$select = selects3;
            q.query.$limit = 200;
            q.query.$sort = { startAt: -1 };
            q.query.$populate = populates3;
            const list = await ctx.service._find(q).catch(err => null);
            ctx.result = list;
            logger.info(`[${connection.userCode}]: [BOOKING]: FIND HISTORY: RESPONSE [${list && list.data.length || 0} items]`);
        }
        return ctx;
    }
}

const findServerTime = function()
{
    return async ctx =>
    {
        const { query = {}, connection } = ctx.params;
        if (query.mode === 'time')
        {
            logger.info(`[${connection.userCode}]: [BOOKING]: GET SERVER TIME`);
            ctx.result = {};
            ctx.result.serverTime = Date.now();
        }
        return ctx;
    }
}

const getBooking = function()
{
    return async ctx =>
    {
        const { query = {}, provider, connection } = ctx.params;
        if (provider)
        {
            const authUser = ctx.params.user;
            logger.info(`[${connection.userCode}]: [BOOKING]: GET ID: [${ctx.id}]`);
            const field = ctx.params.query && ctx.params.query.field;
            const q = {query: {}};
            q.query._id = ctx.id;
            q.query.$or = [{ teacher: authUser._id}, {student: authUser._id}];
            q.query.$select = Array.from(selects);
            q.query.$populate = populates;
            q.query.$limit = 1;
            if (field === 'messages')
            { // additionaal field to return
                q.query.$select.push('messages');
            }
            const bookings = await ctx.service._find(q).catch(err => null);
            ctx.result = bookings && bookings.data && bookings.data[0] || null;
        }
        return ctx;
    }
};

const createBooking = function()
{
    return async ctx =>
    {
        const { connection } = ctx.params;
        logger.info(`[${connection.userCode}]: [BOOKING]: NEW BOOKING HOOK`);
        const ENUM = ctx.app.get('enums');
        const utils = ctx.app.get('utils');
        const User = ctx.app.service('users').Model;
        const Topic = ctx.app.service('topics').Model;
        const Schedule = ctx.app.service('schedules').Model;
        const Payment = ctx.app.service('payments').Model;
        const authUser = ctx.params.user;
        const authUserId = String(authUser._id);

        // validate topic
        const topic = await Topic.findById(ctx.data.topic, 'title state').lean().exec();
        ctx.app.assert(topic, ENUM.ERROR.BadRequest);
        const priceValue = 1;

        // check schedule slot if really empty
        logger.info(`[${connection.userCode}]: [BOOKING]: LOOKING SCHEDULE [${ctx.data.schedule}]`);
        const schedule = await Schedule.findById(ctx.data.schedule, 'startAt endAt state _id').lean().exec();
        ctx.app.assert(schedule, ENUM.ERROR.BadRequest);
        ctx.app.assert(schedule.state === '', ENUM.ERROR.Conflict);

        // validate teacher
        const teacher = await User.findById(ctx.data.teacher, 'role firstName lastName state avatar schedule').lean().exec();
        ctx.app.assert(teacher, ENUM.ERROR.BadRequest);
        ctx.app.assert(teacher.role === ENUM.USERROLE.TEACHER, ENUM.ERROR.BadRequest);
        ctx.app.assert(teacher.state === ENUM.USERSTATE.ACTIVE, ENUM.ERROR.BadRequest);
        const teacherId = String(teacher._id);

        // validate student
        const student = await User.findById(ctx.data.student, 'role firstName lastName state avatar credits _id').exec();
        ctx.app.assert(student, ENUM.ERROR.BadRequest);
        ctx.app.assert(student.role === ENUM.USERROLE.STUDENT, ENUM.ERROR.BadRequest);
        ctx.app.assert(student.state === ENUM.USERSTATE.ACTIVE, ENUM.ERROR.BadRequest);
        ctx.app.assert(String(student._id) === authUserId, ENUM.ERROR.BadRequest);
        const studentId = String(student._id);

        // check credits
        const credits = await utils.getUserCredits(authUserId);
        logger.warn(`[${connection.userCode}]: [BOOKING]: CREDITS: [${credits}], PRICE: [${priceValue}]`);
        ctx.app.assert(credits >= priceValue, ENUM.ERROR.PaymentError);

        // get payment package id to get credits from
        const paymentId = await utils.getPaymentId(authUserId, priceValue);
        logger.info(`[${connection.userCode}]: [BOOKING]: PAYMENT ID: [${paymentId}]`);
        ctx.app.assert(paymentId, ENUM.ERROR.PaymentError);
        const payment = await Payment.findById(paymentId).exec();
        ctx.app.assert(payment, ENUM.ERROR.PaymentError);

        // prepare booking structure
        const data = {};
        data.teacher = ctx.data.teacher;
        data.student = ctx.data.student;
        data.topic = ctx.data.topic;
        data.schedule = ctx.data.schedule;
        data.payment = paymentId;
        data.title = '';
        data.description = '';
        data.difficulty = 0;
        data.icon = '';
        data.paid = true;
        data.confirmed = true;
        data.duration = 0;
        data.events = [];
        data.price = priceValue;
        data.createdAt = new Date();
        data.startAt = schedule.startAt;
        data.endAt = schedule.endAt;
        const booking = await ctx.service._create(data).catch(console.error);
        ctx.app.assert(booking, ENUM.ERROR.BadRequest);

        // subtract credits from payment package
        payment.credits = Number(payment.credits - priceValue);
        const res = await payment.save().then(result => true).catch(console.error);
        if (res)
        { // emit credits change to frontend
            const totalCredits = await utils.getUserCredits(authUserId);
            ctx.app.service('payments').emit('credits', { user: authUserId, credits: totalCredits });
        }
        logger.info(`[${connection.userCode}]: [BOOKING]: PAYMENT ID: [${paymentId}] CURRENT PACKAGE CREDITS: [${payment.credits}]`);

        // add user to relations
        await User.createRelation(teacherId, studentId);
        await User.createRelation(studentId, teacherId);

        // sync users
        ctx.service.emit('sync', { user: [teacherId, studentId] });

        // send notification to teacher
        const title = ENUM.MESSAGE.NOTIFY_BOOKING_NEW_TITLE;
        const temp = ENUM.MESSAGE.NOTIFY_BOOKING_NEW_TEXT;
        const body = utils.evalTemplate(temp,
        {
            STR_ROLE: utils.capitalize(student.role),
            STR_NAME: `${student.firstName} ${student.lastName}`,
            STR_TOPIC: topic.title,
            STR_DATE: dayjs(data.startAt).format('DD.MM.YYYY'),
            STR_TIME: dayjs(data.startAt).format('HH:mm')
        });
        const icon = ''; // `${ctx.app.get('site')}/data/${student.avatar}`;
        const expiry = dayjs(data.startAt).unix();
        await User.sendNotification(teacherId, {title, body, icon, expiry});

        // response
        ctx.result = booking;
        return ctx;
    };
};

const removeBooking = function()
{
    return async ctx =>
    {
        const { connection } = ctx.params;
        const ENUM = ctx.app.get('enums');
        const utils = ctx.app.get('utils');
        const User = ctx.app.service('users').Model;
        const bookingId = ctx.id;
        const Booking = ctx.service.Model;
        const Topic = ctx.app.service('topics').Model;
        const Schedule = ctx.app.service('schedules').Model;
        const Payment = ctx.app.service('payments').Model;
        const authUser = ctx.params.user;
        const authUserId = String(authUser._id);
        logger.info(`[${connection.userCode}]: [BOOKING]: REMOVE BOOKING HOOK [${bookingId}]`);

        // validate booking
        const booking = await Booking.findById(bookingId, 'schedule teacher student topic payment state price startAt').lean().exec();
        ctx.app.assert(booking, ENUM.ERROR.BadRequest);

        // validate ids
        const userId = authUserId;
        const teacherId = String(booking.teacher._id);
        const studentId = String(booking.student._id);
        const scheduleId = String(booking.schedule._id);
        const paymentId = String(booking.payment._id);
        ctx.app.assert((studentId === userId || teacherId === userId), ENUM.ERROR.BadRequest);

        // validate teacher
        const teacher = await User.findById(teacherId, 'role firstName lastName _id').lean().exec();
        ctx.app.assert(teacher, ENUM.ERROR.BadRequest);

        // validate student
        const student = await User.findById(studentId, 'role firstName lastName credits _id').exec();
        ctx.app.assert(student, ENUM.ERROR.BadRequest);

        // validate topic
        const topic = await Topic.findById(booking.topic, 'title state').lean().exec();
        ctx.app.assert(topic, ENUM.ERROR.BadRequest);

        // validate schedule
        const schedule = await Schedule.findById(scheduleId, 'startAt endAt state _id').exec();
        ctx.app.assert(schedule, ENUM.ERROR.BadRequest);

        // set slot as free
        schedule.state = '';
        const slotSave = await schedule.save().then(res => true).catch(err => false);
        ctx.app.assert(slotSave, ENUM.ERROR.GeneralError);

        // classify booking, only future bookings can be removed
        const timestart = new Date(booking.startAt).getTime();
        const timediff = Math.floor((timestart - Date.now()) / 1000);
        const classify = Booking.classifyBooking(timediff);
        logger.info(`[${connection.userCode}]: [BOOKING]: CLASSIFY: [${classify}] [${timediff}] [${timestart}]`);
        ctx.app.assert(classify === 'N' || classify === 'F', ENUM.ERROR.BadRequest);

        // remove booking
        const res = await ctx.service._remove(bookingId).then(result => true).catch(err => false);
        ctx.app.assert(res, ENUM.ERROR.GeneralError);

        // return credits to student
        let returnCredits = false;
        if (authUser.role === ENUM.USERROLE.TEACHER)
        { // teacher request, always return credits to student
            returnCredits = true;
        }
        else if (authUser.role === ENUM.USERROLE.STUDENT)
        { // student request, return only if future booking, not for emergency cancel
            if (classify === 'F') returnCredits = true;
        }
        logger.info(`[${connection.userCode}]: [BOOKING]: PAYMENT RETURN TEST [${returnCredits}]`);

        if (paymentId && returnCredits)
        { // return credits by to student's payment record
            const payment = await Payment.findById(paymentId, 'credits _id').exec();
            if (payment)
            {
                payment.credits = Number(payment.credits + booking.price);
                const rx = await payment.save().then(result => true).catch(console.error);
                if (rx)
                {
                    const totalCredits = await utils.getUserCredits(authUserId);
                    ctx.app.service('payments').emit('credits', { user: authUserId, credits: totalCredits });
                }
                logger.info(`[${connection.userCode}]: [BOOKING]: PAYMENT ID: [${paymentId}] RETURN TO CREDITS: [${payment.credits}]`);
            }
        }

        // sync users
        ctx.service.emit('sync', { user: [teacherId, studentId] });

        // send notification
        let sourceUser;
        let targetUser;
        if (userId == studentId)
        { // booking cancelled by student
            sourceUser = student;
            targetUser = teacher;
        }
        else
        { // booking cancelled by teacher
            sourceUser = teacher;
            targetUser = student;
        }
        const title = ENUM.MESSAGE.NOTIFY_BOOKING_CANCEL_TITLE;
        const temp = ENUM.MESSAGE.NOTIFY_BOOKING_CANCEL_TEXT;
        const body = utils.evalTemplate(temp,
        {
            STR_ROLE: utils.capitalize(sourceUser.role),
            STR_NAME: `${sourceUser.firstName} ${sourceUser.lastName}`,
            STR_TOPIC: topic.title,
            STR_DATE: dayjs(booking.startAt).format('DD.MM.YYYY'),
            STR_TIME: dayjs(booking.startAt).format('HH:mm')
        });
        const icon = ''; // `${ctx.app.get('site')}/data/${student.avatar}`;
        const expiry = dayjs(booking.startAt).unix();
        await User.sendNotification(targetUser._id, {title, body, icon, expiry});

        // response
        ctx.result = res;
        return ctx;
    };
};

const updateBooking = function()
{
    return async ctx =>
    {
        const { connection } = ctx.params;
        const ENUM = ctx.app.get('enums');
        if (ctx.data.action !== undefined)
        { // action defined, update
            const result = {};
            let calcRating = false;
            const authUser = ctx.params.user;
            const User = ctx.app.service('users').Model;
            const Booking = ctx.service.Model;
            const bookingId = ctx.id;
            const userId = String(authUser._id);

            logger.info(`[${connection.userCode}]: [BOOKING]: ID: [${ctx.id}], STORE EVENT: ${ctx.data.action}`);

            // validate booking
            const booking = await Booking.findById(bookingId, 'events messages teacher student seenTimes state').exec();
            ctx.app.assert(booking, ENUM.ERROR.BadRequest);

            // validate user
            const studentId = String(booking.student._id);
            const teacherId = String(booking.teacher._id);
            const isValidUser = (studentId === userId || teacherId === userId);
            ctx.app.assert(isValidUser, ENUM.ERROR.BadRequest);

            if (ctx.data.action === 'rating')
            { // set rating (student only)
                // TODO: Check class time too
                logger.info(`[${connection.userCode}]: [BOOKING]: ID: [${ctx.id}], STORE RATING: ${ctx.data.value}`);
                ctx.app.assert((studentId === userId), ENUM.ERROR.BadRequest);
                const rating = validator.sanitizeValue('integer', ctx.data.value, 0, 0, 5);
                booking.rating = rating;
                calcRating = true;
            }

            if (ctx.data.action === 'seen')
            {
                logger.info(`[${connection.userCode}]: [BOOKING]: ID: [${ctx.id}], STORE SEEN TIME`);
                const times = booking.seenTimes || [];
                // times.push({ seenAt: new Date(), user: authUser._id });
                const seenData = times.find(item => item.user.equals(userId));
                if (seenData)
                { // update record
                    seenData.seenAt = new Date();
                }
                else
                { // new record
                    times.push({ seenAt: new Date(), user: authUser._id });
                }
                booking.seenTimes = times;
            }

            if (ctx.data.action === 'event')
            { // push new event
                const events = booking.events || [];
                events.push({ action: ctx.data.value, createdAt: new Date(), user: authUser._id });
                booking.events = events;
            }

            if (ctx.data.action === 'message')
            { // push new chat message
                const messages = booking.messages || [];
                const message = validator.sanitizeValue('string', ctx.data.value);
                const timestamp = new Date();
                messages.push({ text: message, createdAt: timestamp, user: authUser._id });
                booking.messages = messages;
                let from, to;
                if (userId === studentId)
                {
                    from = studentId;
                    to = teacherId;
                }
                else if (userId === teacherId)
                {
                    from = teacherId;
                    to = studentId;
                }
                if (from)   ctx.service.emit('chat', { booking: bookingId, action: 'message', from: from, to: to, message: message, timestamp: timestamp.getTime() });
            }

            // store
            const res = await booking.save().then(res => true).catch(console.error);
            ctx.app.assert(res, ENUM.ERROR.GeneralError);

            // additional tasks
            if (calcRating) await User.updateTeacherRating(teacherId);

            // response
            ctx.result = result;
        }
        return ctx;
    };
};

const changeScheduleState = function(slotState)
{
    return async ctx =>
    {
        const { connection } = ctx.params;
        logger.info(`[${connection.userCode}]: [BOOKING]: UPDATE SCHEDULE SLOT [${slotState}]`);
        if (ctx.data.schedule && ctx.data.teacher)
        {
            const Schedule = ctx.app.service('schedules').Model;
            await Schedule.updateOne({ _id: ctx.data.schedule, teacher: ctx.data.teacher }, { $set: { state: slotState } }).exec();
        }
        ctx.service.emit('sync', { user: String(ctx.data.teacher) });
        return ctx;
    };
};

module.exports = {

    before:
    {
        all:    [ authenticateUser() ],
        find:   [ findTeacherBookings(), findStudentBookings(), findHistory(), findServerTime(), disallowApi() ],
        get:    [ getBooking(), disallowApi() ],
        create: [ createBooking(), disallowApi() ],
        update: [ disallowApi() ],
        patch:  [ updateBooking(), disallowApi() ],
        remove: [ removeBooking(), disallowApi() ]
    },

    after:
    {
        all:    [],
        find:   [],
        get:    [],
        create: [ changeScheduleState('c') ],
        update: [],
        patch:  [],
        remove: []
    },

    error:
    {
        all:    [],
        find:   [],
        get:    [],
        create: [],
        update: [],
        patch:  [],
        remove: []
    }
};
