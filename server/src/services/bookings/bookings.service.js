// Initializes the `bookings` service on path `/bookings`
const { Bookings } = require('./bookings.class');
const createModel = require('../../models/bookings.model');
const hooks = require('./bookings.hooks');
const logger = require('../../core/logger');

module.exports = function (app)
{
    const options = {
        Model: createModel(app),
        paginate: app.get('paginate'),
        events: ['chat', 'sync']
    };

    // Initialize our service with any options it requires
    app.declareService('bookings', new Bookings(options, app));
    //app.use('/bookings', new Bookings(options, app));

    // Get our initialized service so that we can register hooks
    const service = app.getService('bookings');
    service.hooks(hooks);


    service.publish && service.publish('chat', (data, ctx) =>
    { // process "chat" event
        let toChannels = [];
        if (data && data.action === 'message')
        { // send message event to booking student and teacher only
            if (data.from)     toChannels.push(ctx.app.channel(`userIds/${data.from}`));
            if (data.to)    toChannels.push(ctx.app.channel(`userIds/${data.to}`));
        }
        return toChannels;
    });

    service.publish && service.publish('sync', (data, ctx) =>
    { // process "sync" event
        let toChannels = [];
        if (data && data.user)
        { // send message event to user(s) only
            //toChannels.push(ctx.app.channel(`userIds/${data.user}`));
            const arr = Array.isArray(data.user) ? data.user.map(userId => ctx.app.channel(`userIds/${userId}`)) : [ctx.app.channel(`userIds/${data.user}`)];
            toChannels.push(...arr);
        }
        return toChannels;
    });

};
