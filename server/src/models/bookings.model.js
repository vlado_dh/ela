
module.exports = function (app)
{
    const modelName = 'bookings';
    const mongoose = app.get('mongoose');
    const ObjectId  = mongoose.Schema.Types.ObjectId;
    const ENUM = app.get('enums');

    const eventData = new mongoose.Schema(
    {
        action:     { type: 'string', default: ''},
        user:       { type: ObjectId, ref: 'users'},
        createdAt:  { type: Date, default: null }
    }, { _id: false });

    const messageData = new mongoose.Schema(
    {
        text:       { type: 'string', default: ''},
        user:       { type: ObjectId, ref: 'users'},
        createdAt:  { type: Date, default: null },
    }, { _id: false });

    const seenData = new mongoose.Schema(
    {
        user:       { type: ObjectId, ref: 'users'},
        seenAt:     { type: Date, default: null },
    }, { _id: false });

    const schema = new mongoose.Schema(
    {
        title:          { type: String, maxlength: 255 },
        description:    { type: String, maxlength: 65535 },
        schedule:       { type: ObjectId, ref: 'schedules', required: true },
        teacher:        { type: ObjectId, ref: 'users', required: true },
        student:        { type: ObjectId, ref: 'users', required: true },
        topic:          { type: ObjectId, ref: 'topics', required: true },
        comment:        { type: ObjectId, ref: 'comments', default: null },
        payment:        { type: ObjectId, ref: 'payments', default: null },
        rating:         { type: Number, default: 0, min: 0, max: 5 },
        difficulty:     { type: Number, default: 0, min: 0, max: 9 },
        price:          { type: Number, default: 1, min: 0, max: 9999 },
        icon:           { type: String },
        confirmed:      { type: Boolean, default: false },
        completed:      { type: Boolean, default: false },
        paid:           { type: Boolean, default: false },
        state:          { type: String, default: '' },
        duration:       { type: Number, default: 0, min: 0 },
        messages:       { type: [messageData], detault: []},
        events:         { type: [eventData], detault: []},
        seenTimes:      { type: [seenData], default: []},
        startAt:        { type: Date, default: null },
        endAt:          { type: Date, default: null },
        startedAt:      { type: Date, default: null },
        stoppedAt:      { type: Date, default: null },
        createdAt:      { type: Date, default: Date.now },
        updatedAt:      { type: Date, default: Date.now }
    });

    schema.index({ teacher: 1});
    schema.index({ student: 1});
    schema.index({ topic: 1});
    schema.index({ startAt: 1});

    schema.virtual('_teacher_',
    {
        localField: 'teacher',
        ref: 'users',
        foreignField : '_id',
        justOne: true
    });

    schema.virtual('_student_',
    {
        localField: 'student',
        ref: 'users',
        foreignField : '_id',
        justOne: true
    });

    schema.virtual('_topic_',
    {
        localField: 'topic',
        ref: 'topics',
        foreignField : '_id',
        justOne: true
    });

    schema.statics.classifyBooking = function(timediff)
    {
        let result;
        if (timediff < -1800)         result = 'P'; // PAST
        else if (timediff < 0)        result = 'A'; // ACTUAL / LIVE
        else if (timediff < 43200)    result = 'N'; // NEAR (12 hrs)
        else                          result = 'F'; // FUTURE
        return result;
    };

    if (mongoose.modelNames().includes(modelName))    mongoose.deleteModel(modelName);
    return mongoose.model(modelName, schema);
};
