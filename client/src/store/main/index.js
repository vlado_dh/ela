
import app from '../../api/feathers';
import { getTimeObject } from '../../api/utils';
import Vue from 'vue';
import { SessionStorage } from 'quasar';
import { computeFingerprint } from 'simple-fingerprint';
import { i18n } from 'boot/i18n.js';

const store = {namespaced: true};

store.state = {
    headerTitle: 'Good Day English',
    toolbarMessage: '',
    backNavigation: '/',
    drawerEnabled: true,
    drawerOpened: false,
    dayIndex: 0,
    serverTime: 0,
    startTime: Date.now(),
    currentTime: Date.now(),
    offsetTime: 0,
    onlineState: false,
    busyState: false,
    userCredits: 0,
    fingerprint: '',
    teacherMenu: [
        { label: i18n.t('message.title_confirmedbookings'), icon: 'school', link: 'teacherBookings', badge: 'bookingsCount' },
        { label: i18n.t('message.title_schedulecancel'), icon: 'alarm_on', link: 'teacherSchedule' },
        { label: i18n.t('message.title_comments'), icon: 'comment', link: 'teacherComments' },
        { label: i18n.t('message.title_timetable'), icon: 'today', link: 'teacherTimetable' },
        { label: i18n.t('message.title_alltopics'), icon: 'emoji_flags', link: 'teacherTopics' },
        /* { label: i18n.t('message.title_payments'), icon: 'payment', link: 'teacherPayments' }, */
        { label: i18n.t('message.title_mystudents'), icon: 'perm_contact_calendar', link: 'teacherStudents' },
        { label: i18n.t('message.title_myprofile'), icon: 'face', link: 'teacherProfile' },
        { label: i18n.t('message.title_logout'), icon: 'logout', link: 'logout' }
    ],
    studentMenu: [
        { label: i18n.t('message.title_upclasses'), icon: 'school', link: 'studentUpClasses' },
        { label: i18n.t('message.title_bookcancel'), icon: 'access_alarm', link: 'studentBooking' },
        { label: i18n.t('message.title_myclasses'), icon: 'record_voice_over', link: 'studentClasses' },
        { label: i18n.t('message.title_alltopics'), icon: 'emoji_flags', link: 'studentTopics' },
        { label: i18n.t('message.title_comments'), icon: 'comment', link: 'studentComments' },
        { label: i18n.t('message.title_teachers'), icon: 'people_alt', link: 'studentTeachers' },
        { label: i18n.t('message.title_payments'), icon: 'payment', link: 'studentPayments' },
        { label: i18n.t('message.title_myprofile'), icon: 'face', link: 'studentProfile' },
        { label: i18n.t('message.title_logout'), icon: 'logout', link: 'logout' }
    ]
};

store.getters = {
    dayIndex: function(state)
    {
        const idx = state.dayIndex || SessionStorage.getItem('main/dayIndex') || '0';
        return String(idx);
    },
    currentTimeStamp: function(state)
    {
        return state.currentTime;
    },
    serverTimeStamp: function(state)
    {
        return (state.currentTime - state.offsetTime) + state.serverTime;
    },
    runTime: function(state)
    {
        const diff = Date.now() - state.startTime;
        return Math.floor(diff / 1000 % 60);
    },
    currentTime: function(state)
    {
        const serverStamp = (state.currentTime - state.offsetTime) + state.serverTime;
        return `${new Date(serverStamp).toLocaleTimeString()}`;
    },
    userCredits: function(state)
    {
        return state.userCredits;
    },
    fingerprint: function(state)
    {
        return state.fingerprint;
    }
};

store.actions = {

    initialize: async function({ commit, state })
    {
        // setup time management
        commit('offsetTime', 0);
        let oldTime = Date.now();
        setInterval(() =>
        { // current time
            const sec = getTimeObject().unix();
            const root = Vue.$app.router.app;
            const time = Date.now();
            // const sec = Math.floor(time / 1000);
            if (!(sec % 1800))
            { // emit every 30 minutes
                root && root.$emit('evt-class-tick');
            }
            if (Math.abs(time - oldTime) > 2000)
            { // emit when time change detected
                root && root.$emit('evt-time-change');
            }
            commit('currentTime', time);
            oldTime = time;
        }, 1000);

        // setup credits state watcher
        app.service('payments').on('credits', async data =>
        { // watcher
            if (data && data.credits) commit('userCredits', data.credits);
        });

        // calc fingerprint
        const fingerprint = await computeFingerprint().catch(err => '');
        commit('fingerprint', fingerprint);
    },
    syncServerTime: async function({ commit })
    {
        const serverTime = await app.service('system').find({ query: {mode: 'time'} }).then(res => res && res.serverTime || 0).catch(err => false);
        if (serverTime) commit('serverTime', serverTime);
    },
    syncUserCredits: async function({ commit })
    {
        const cr = await app.service('payments').find({ query: {mode: 'credits'} }).then(res => res && res.credits || 0).catch(err => false);
        commit('userCredits', cr);
    }
};

store.mutations = {

    headerTitle: function(state, value)
    {
        state.headerTitle = value;
    },
    toolbarMessage: function(state, value)
    {
        state.toolbarMessage = value;
    },
    dayIndex: function(state, value)
    {
        state.dayIndex = value;
        SessionStorage.set('main/dayIndex', value);
    },
    serverTime: function(state, value)
    {
        state.offsetTime = Date.now();
        state.serverTime = value;
    },
    currentTime: function(state, value)
    {
        state.currentTime = value;
    },
    offsetTime: function(state, value)
    {
        state.offsetTime = value;
    },
    backNavigation: function(state, value)
    {
        state.backNavigation = value;
    },
    drawerEnabled: function(state, value)
    {
        state.drawerEnabled = value;
    },
    drawerOpened: function(state, value)
    {
        state.drawerOpened = value;
    },
    onlineState: function(state, value)
    {
        state.onlineState = value;
    },
    busyState: function(state, value)
    {
        state.busyState = value;
    },
    userCredits: function(state, value)
    {
        state.userCredits = value || 0;
    },
    fingerprint: function(state, value)
    {
        state.fingerprint = value || '';
    }
};

export default store;
